class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def demo
    render text: "The environment is #{Rails.env}, Daya test setting is #{ENV['EXTRA_SETTING']}. The time is #{Time.now.strftime('%b %d %Y %H:%M:%S')}"
  end
end
